#!/bin/bash -xe

cd "$(dirname "$0")"
echo $(pwd)

git checkout asciibooks-01
git checkout asciibooks-02
git checkout asciibooks-03
git checkout asciibooks-04
git checkout asciibooks-05
git checkout asciibooks-06
git checkout asciibooks-07
git checkout asciibooks-08
git checkout master

previousBranch=master
for branch in $(git branch | grep -v master)
do
  # TODO: Check if branch is clean
  echo $branch
  rm -rf ../$branch
  git checkout $branch
  git merge $previousBranch
  mkdir ../$branch
  cp -R * ../$branch
  previousBranch=$branch
done
