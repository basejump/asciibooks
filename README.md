# AsciiBooks

This Grails app contains all the code for the second 1/2 of the https://www.grails3book.com book.

# Download

You can use the download links above, or download a zip here: https://gitlab.com/grails-3-book/asciibooks/repository/archive.zip

# How branches are kept up-to-date

Each branch is merged up when changes are made to the previous, for example:

For example:  Grails update to 3.2.8 goes into master
Then:
* asciibooks-01 is rebased on master
* asciibooks-02 is rebased on asciibooks-01
* asciibooks 03 is rebased on asciibooks-02
* ...

See checkout.sh for how branches are merged. checkout.sh also copies each brach into a sepreate folder for asciidoctor to read in contents to the book.
